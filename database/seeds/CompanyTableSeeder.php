<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'Nombre Empresa',
            'address' => 'Direccion de la empresa',
            'about' => 'Texto corto acerca de la empresa',
            'provincia' => 'Provincia',
            'canton' => 'Canton',
            'teluno' => '00000000',
            'teldos' => '00000000',
            'image' => 'logo.png'
        ]);
    }
}
