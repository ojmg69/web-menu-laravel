<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [ 
                [
                    'name' => 'Hamburguesa Premiere',
                    'description' => 'Pan, Torta, Tomate, lechuga , pepinillos, queso amarilloy papas',
                    'options' => 'Opciones del producto',
                    'extras' => 'Extras Del Producto',
                    'price' => 2500,
                    'image' => 'image1.png',
                    'active' => '1',
                    'category_id' => '1',
                ],
                [
                    'name' => 'Hamburguesa Normal',
                    'description' => 'Pan, Torta, Tomate, lechuga , pepinillos, queso amarilloy papas',
                    'options' => 'Opciones del producto',
                    'extras' => 'Extras Del Producto',
                    'price' => 2000,
                    'image' => 'image2.png',
                    'active' => '1',
                    'category_id' => '2',
                    ],
            ]   
    );
    }
}
