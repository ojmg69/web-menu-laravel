@extends('adminlte::page')

@section('title', 'Categoria')

@section('content')
<div class="card">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-3 mt-4"><a class="btn btn-success" href="{{url('categoria/create')}}"><i
                    class="fas fa-plus"></i> Agregar</a>
            </br>
        </div>
        <div class="col-md-10">

            <h2><b>Lista de Categorías</b> </h2>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr class="text-center">
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categorias as $categoria)
                    <tr>
                        <th scope="row">{{ $categoria->id }}</th>
                        <td>{{ $categoria->name }}</td>
                        <td>No Asociada</td>
                        <td><button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
                                data-target="#exampleModal">
                                <i class="fas fa-trash-alt"></i> Eliminar</button>
                        </td>
                    </tr>
                    @endforeach
                    @if(!$con_categorias->isEmpty())
                    @foreach ($con_categorias as $con_categoria)
                    <tr>
                        <th scope="row">{{ $con_categoria->id }}</th>
                        <td>{{ $con_categoria->name }}</td>
                        <td><i class="fas fa-thumbtack"></i> Asociada</td>
                        <td> <a class="btn btn-sm btn-primary" href="{{url('categoria/'.$con_categoria->id.'/edit')}}"
                                alt="Editar categoria"><i class="fas fa-edit"></i> Modificar</a></td>
                    </tr>

                    @endforeach
                    @endif

                </tbody>

            </table>
            {{ $categorias->links() }}



        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="text-danger fas fa-exclamation-triangle"></i>
                    Notificación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta Seguro que quiere eliminar?
            </div>
            <div class="modal-footer">
                <form action="{{ route('categoria.destroy', $categoria->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-primary" type="submit">Confirmar</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
console.log('Hi!');
</script>
@stop