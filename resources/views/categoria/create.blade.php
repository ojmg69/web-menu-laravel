@extends('adminlte::page')
@section('title', 'Agregar Categoria')
@section('content')
<div class="">
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
  </div>
@endif
    <div class="row justify-content-center">
        <div class="col-md-10 mb-3 mt-4"><a class="btn btn-primary" href="{{url('categoria')}}"><i class="fas fa-long-arrow-alt-left"></i> Regresar</a>
            </br>
        </div>
        <div class="col-md-10 mb-2 mt-2">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h2 class="card-title"><b>Agregar Categoría</b> </h2>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(['route' => 'categoria.store','method' => 'post','class'=>'p-3']) !!}

        <div class="row">
        <div class="col-md-12 form-group">
        {!! Form::label('name','Nombre:')!!}
        {!! Form::text('name','',['id'=>'name','class'=>'form-control'])!!}
        </div>
        </div>
 
       
        <button class='btn btn-success' type='submit' value='submit'>
    <i class='fas fa-pencil-alt'> </i> Agregar
</button>
        {!! Form::close() !!}
            </div>
            <!-- /.card -->

            </div>
        </div>
    </div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
console.log('Hi!');
</script>
@stop