@extends('adminlte::page')

@section('title', 'Detalle Producto')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="col-md-8 mb-4">
      <a class="btn btn-primary " href="{{url('producto')}}">Volver a la lista</a>
    </div>
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title font-weight-bold">Detalle Producto</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body box-profile">
      <div class="row">
    
      <div class="col-md-6">
      <h4 class="font-weight-bold text-center"> {{$product->name}}</h4>
      <div class="form-group">
          <label>Descripción:</label><br/>
          {{$product->description}}
        </div>

        <div class="form-group">
          <label>Precio:</label>
          {{ number_format($product->price, 2) }} $
        </div>
        <div class="form-group">
          <label>Categoría:</label>
          {{$product->category}}
        </div>

        <div class="form-group">
          <label>Disponible:</label>
          @if($product->active==1)
                                SI
                                @else
                                NO
                                @endif
        </div>
      </div>

      <div class="col-md-6">
      <img src="{{asset('img/products/'.$product->image)}}" alt="...">
      </div>
      </div>
      </div>
    </div>
  </div>
</div>


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
  
</script>
@stop