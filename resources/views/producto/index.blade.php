@extends('adminlte::page')

@section('title', 'Productos')

@section('content')
<div class="card">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-3 mt-4"><a class="btn btn-success" href="{{url('producto/create')}}"><i class="fas fa-plus"></i> Agregar</a>
            </br>
        </div>
        <div class="col-md-10">

            <h2><b>Lista de Productos</b> </h2>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr class="text-center">
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Categoría</th>
                        <th scope="col">Disponible</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <th scope="row">{{ $product->name }}</th>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->category }}</td>
                            <td class="text-center">
                                @if($product->active==1)
                                SI
                                @else
                                NO
                                @endif
                            </td>
                            <td width=270 > 
                            <a class="btn btn-sm btn-info" href="{{url('producto/'.$product->id.'/show')}}"
                                alt="Ver producto"><i class="fas fa-eye"></i> Ver</a>
                            <a class="btn btn-sm btn-primary" href="{{url('producto/'.$product->id.'/edit')}}"
                                alt="Editar producto"><i class="fas fa-edit"></i> Modificar</a>
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
                                data-target="#exampleModal">
                                <i class="fas fa-trash-alt"></i> Eliminar</button>
                    
                        </tr>
                    @endforeach
                </tbody>

            </table>
            {{ $products->links() }}


        </div>
        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="text-danger fas fa-exclamation-triangle"></i>
                    Notificación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta Seguro que quiere eliminar?
            </div>
            <div class="modal-footer">
                <form action="{{ route('producto.destroy', $product->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-primary" type="submit">Confirmar</button>
                </form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
console.log('Hi!');
</script>
@stop