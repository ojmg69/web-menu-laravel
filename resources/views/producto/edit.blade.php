@extends('adminlte::page')
@section('title', 'Agregar Producto')
@section('content')
<div class="">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12"><a class="btn btn-primary" href="{{url('producto')}}"><i class="fas fa-long-arrow-alt-left"></i> Regresar</a>
            </br>
        </div>
        <div class="col-md-12 mb-2 mt-2">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h2 class="card-title"><b>Agregar Producto</b> </h2>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'producto.update','method' => 'post','class'=>'p-3','enctype'=>'multipart/form-data']) !!}

                <div class="row">
                {!! Form::hidden('product_id',$product->id)!!}
                    <div class="col-md-6 form-group">
                        {!! Form::label('name','Nombre:')!!}
                        {!! Form::text('name',$product->name,['id'=>'name','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('description','Descripción:')!!}
                        {!! Form::text('description',$product->description,['id'=>'description','class'=>'form-control'])!!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('options','Opciones:')!!}
                        {!! Form::text('options',$product->options,['id'=>'options','class'=>'form-control'])!!}
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('extras','Extras:')!!}
                        {!! Form::text('extras',$product->extras,['id'=>'extras','class'=>'form-control'])!!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 form-group">
                        {!! Form::label('price','Precio:')!!}
                        {!! Form::number('price',$product->price,['id'=>'price','class'=>'form-control'])!!}
                    </div>

                    <div class="col-md-4 form-group">
                        {!! Form::label('category_id','Categoria:')!!}
                        {!! Form::select('category_id',$categorias , $product->category_id,['id'=>'category_id','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('active','Disponible:')!!}
                        {!! Form::select('active',['1'=>'SI','0'=>'NO'] , $product->active,['id'=>'active','class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="row">
                <div class="col-md-6 form-group">
                <input type="hidden" id="Imagecroped" name="Imagecroped">
                        {!! Form::label('imagen','Selecciona una imagen:')!!}
                        <input type="file" class="item-img file" id="imagen" name="imagen"> 
                    </div>
                     <img src="" class="gambar" id="item-img-output" />
                     <span class="help-inline"></span>
                          
     
                </div>


                <button class='btn btn-success' type='submit' value='submit'>
                    <i class='fas fa-pencil-alt'> </i> Agregar
                </button>
                {!! Form::close() !!}
            </div>
            <!-- /.card -->

        </div>
    </div>
</div>

<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">
    
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="upload-demo" class="center-block"></div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="cropImageBtn" class="btn btn-primary">Crear</button>
                    </div>
                </div>
            </div>
        </div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<style>
.modal-lg, .modal-xl {
    max-width: 535px;
}
</style>
@stop

@section('js')
<script>
            var c = new Croppie(document.getElementById('item'));
            // call a method
            c.method(args);
        </script>

        <script>
            // Start upload preview image
            $(".gambar").attr("src", "{{asset('img/products/'.$product->image.'')}}");
            var $uploadCrop,
                tempFilename,
                rawImg,
                imageId;
    
            function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.upload-demo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawImg = e.target.result;
                        
                    }
                    //reader.src = URL.createObjectURL(input.files[0]);
                    reader.readAsDataURL(input.files[0]);
                        
                } else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
            }
    
            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 500,
                    height: 300,
                },
                boundary: {
                    width: 500,
                    height: 300
                },
                enableExif: true
            });
            $('#cropImagePop').on('shown.bs.modal', function() {
                // alert('Shown pop');
                $uploadCrop.croppie('bind', {
                    url: rawImg
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            });
    
            $('.item-img').on('change', function() {
                //imageId = $(this)[0].data('id');
                imageId = $(this)[0].id;
                
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                console.log( $('#cancelCropBtn').data('id', imageId));
                readFile(this);
            });
            $('#cropImageBtn').on('click', function(ev) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'png',
                    size: {
                        width: 500,
                        height: 300
                    }
                    
                }).then(function(resp) {
                    $('#item-img-output').attr('src', resp);
                    $('#cropImagePop').modal('hide');
    
                var img = resp;
                anchor = $("#Imagecroped");
                anchor.val(img);
                    
        
                });
            });
        </script>
@stop