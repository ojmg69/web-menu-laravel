@extends('adminlte::page')
@section('title', 'Agregar Categoria')
@section('content')
<div class="">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12 mb-1 mt-1"><a class="btn btn-primary" href="{{url('perfil')}}"><i
                    class="fas fa-long-arrow-alt-left"></i> Regresar</a>
            </br>
        </div>
        <div class="col-md-12 mb-2 mt-1">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h2 class="card-title"><b>Editar Perfil</b> </h2>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'perfil.update','method' => 'post','class'=>'p-3','enctype'=>'multipart/form-data']) !!}

                <div class="row">
                    {!! Form::hidden('perfil_id',$company->id)!!}
                    <div class="col-md-4 form-group">
                        {!! Form::label('name','Nombre:')!!}
                        {!! Form::text('name',$company->name,['id'=>'name','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('about','Descripción:')!!}
                        {!! Form::text('about',$company->about,['id'=>'about','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('address','Direccion:')!!}
                        {!! Form::text('address',$company->address,['id'=>'address','class'=>'form-control'])!!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 form-group">
                        {!! Form::label('provincia','Provincia:')!!}
                        {!! Form::text('provincia',$company->provincia,['id'=>'provincia','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-3 form-group">
                        {!! Form::label('canton','Cantón:')!!}
                        {!! Form::text('canton',$company->canton,['id'=>'canton','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-3 form-group">
                        {!! Form::label('teluno','Telefono Uno:')!!}
                        {!! Form::text('teluno',$company->teluno,['id'=>'teluno','class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-3 form-group">
                        {!! Form::label('teldos','Telefono Dos:')!!}
                        {!! Form::text('teldos',$company->teldos,['id'=>'teldos','class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="row">
                <div class="col-md-3 form-group">
                <input type="hidden" id="Imagecroped" name="Imagecroped">
                        {!! Form::label('imagen','Selecciona una imagen:')!!}
                        <input type="file" class="item-img file" id="imagen" name="imagen"> 
                    </div>
                     <img src="" class="gambar" id="item-img-output" />
                     <span class="help-inline"></span>
                          
     
                </div>

                   <button class='btn btn-success' type='submit' value='submit'>
                    <i class='fas fa-pencil-alt'> </i> Actualizar
                </button>
                {!! Form::close() !!}
            </div>
            <!-- /.card -->

        </div>
    </div>
</div>

<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">
    
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="upload-demo" class="center-block"></div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="cropImageBtn" class="btn btn-primary">Crear</button>
                    </div>
                </div>
            </div>
        </div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">


@stop

@section('js')
<script>
            var c = new Croppie(document.getElementById('item'));
            // call a method
            c.method(args);
        </script>

        <script>
            // Start upload preview image
            $(".gambar").attr("src", "{{asset('img/profile/'.$company->image.'')}}");
            var $uploadCrop,
                tempFilename,
                rawImg,
                imageId;
    
            function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.upload-demo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawImg = e.target.result;
                        
                    }
                    //reader.src = URL.createObjectURL(input.files[0]);
                    reader.readAsDataURL(input.files[0]);
                        
                } else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
            }
    
            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 250,
                    height: 170,
                },
                boundary: {
                    width: 250,
                    height: 170
                },
                enableExif: true
            });
            $('#cropImagePop').on('shown.bs.modal', function() {
                // alert('Shown pop');
                $uploadCrop.croppie('bind', {
                    url: rawImg
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            });
    
            $('.item-img').on('change', function() {
                //imageId = $(this)[0].data('id');
                imageId = $(this)[0].id;
                console.log(imageId);
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                console.log( $('#cancelCropBtn').data('id', imageId));
                readFile(this);
            });
            $('#cropImageBtn').on('click', function(ev) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'png',
                    size: {
                        width: 250,
                        height: 170
                    }
                    
                }).then(function(resp) {
                    $('#item-img-output').attr('src', resp);
                    $('#cropImagePop').modal('hide');
    
                var img = resp;
                anchor = $("#Imagecroped");
                anchor.val(img);
                    
        
                });
            });
        </script>
@stop