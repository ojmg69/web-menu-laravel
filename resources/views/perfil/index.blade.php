@extends('adminlte::page')

@section('title', 'Perfil')

@section('content')
<div class="row offset-md-1"> 
            <div class="col-md-6">
                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold">Perfil Empresa</h3>
                    </div>
                    <!-- /.card-header -->
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="img-thumbnail" style="max-width: 250px;
    height: auto;"
                                    src="{{asset('img/profile/'.$company->image.'')}}"
                                    alt="User profile picture">
                            </div>
                        <div class="col text-center">
                            <h3 class="font-weight-bold">{{$company->name}}</h3>
                        </div>
                        <br />
                        <strong><i class="fas fa-book mr-1"></i> Descripción</strong>

                        <p class="text-muted">
                            {{$company->about}}
                        </p>
                        <hr>
                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>
                        <p class="text-muted">{{$company->address}}</p>
                        <hr>
                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Provincia</strong>
                        <p class="text-muted">{{$company->provincia}}</p>
                        <hr>
                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Cantón</strong>
                        <p class="text-muted">{{$company->canton}}</p>
                       <hr>
                        <strong><i class="fas fa-phone-alt mr-1"></i> Telefonos</strong>

                        <p class="text-muted"> {{$company->teluno}} - {{$company->teldos}}  </p>
                        <hr>
                        <strong><i class="fas fa-envelope"></i> Email Administrador</strong>

                        <p class="text-muted"> {{$user_email}}   </p>
                    </div>
                    <div class="col pb-2">
                    <a class="btn btn-primary" href="{{url('perfil/'.$company->id.'/edit')}}"
                                alt="Modificar Empresa"><i class="fas fa-edit"></i> Modificar</a>
                    </div>
                    
                </div>
                <!-- /.card -->
            </div>

            </div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
console.log('Hi!');
</script>
@stop