@extends('adminlte::page')

@section('title', 'Panel')

@section('content_header')
    <h1>Panel Administrativo</h1>
@stop

@section('content')
<div class="row">
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$total_productos}}</h3>

                <p>Productos</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a href="{{url('/producto')}}" class="small-box-footer">
                Ver Más <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$total_categoria}}</h3>

                <p>Categorias</p>
              </div>
              <div class="icon">
                <i class="fas fa-fw fa-tags"></i>
              </div>
              <a href="{{url('/categoria')}}" class="small-box-footer">
                Ver Más <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
        </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop