@extends('adminlte::page')
@section('title', 'Agregar Categoria')
@section('content')
<div class="">
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
  </div>
@endif
    <div class="row justify-content-center">
        <div class="col-md-10 mb-3 mt-4"><a class="btn btn-primary" href="{{url('categoria')}}"><i class="fas fa-long-arrow-alt-left"></i> Regresar</a>
            </br>
        </div>
        <div class="col-md-10 mb-2 mt-2">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h2 class="card-title"><b>Cambiar Contraseña</b> </h2>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(['route' => 'contrasena.update','method' => 'post','class'=>'p-3']) !!}

        <div class="row">
        {!! Form::hidden('user_id',$user_id)!!}
        <div class="col-md-12 form-group">
        {!! Form::label('email','Email:')!!}
        {!! Form::email('email',$user_email,['id'=>'email','class'=>'form-control'])!!}
        </div>
        <div class="col-md-12 form-group">
        {!! Form::label('password','Contraseña:')!!}
        {!! Form::password('password',['id'=>'password','class'=>'form-control'])!!}
        </div>
        </div>
 
       
        <button class='btn btn-success' type='submit' value='submit'>
    <i class='fas fa-pencil-alt'> </i> Actualizar
</button>
        {!! Form::close() !!}
            </div>
            <!-- /.card -->

            </div>
        </div>
    </div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
console.log('Hi!');
</script>
@stop