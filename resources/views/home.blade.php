@extends('layouts.app')

@section('content')
<div class="container site">
    <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span> {{$company}} <span class="glyphicon glyphicon-cutlery"></span></h1>
    <nav>
        <ul class="nav nav-pills">

            @foreach ($categories as $category)
            @if($category->id==1)

            <li role="presentation" class="active"><a href="#{{$category->id}}" data-toggle="tab">{{$category->name}}</a></li>
            @else
            <li role="presentation"><a href="#{{$category->id}}" data-toggle="tab">{{$category->name}}</a></li>
            @endif
            @endforeach

        </ul>
    </nav>

    <div class="tab-content">
        @foreach ($categories as $category)
        @if($category->id==1)

        <div class="tab-pane active" id="{{$category->id}}">
            @else
            <div class="tab-pane" id="{{$category->id}}">
                @endif
                <div class="row">
                    @foreach ($products as $product)
                    @if($category->id==$product->category_id)
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <img src="{{asset('img/products/'.$product->image)}}" alt="...">
                            <div class="price">{{ number_format($product->price, 2) }} $</div>
                            <div class="caption">
                                <h4>{{$product->name}}</h4>
                                <p>{{$product->description}}</p>
                                <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Ordenar</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>

    </div>
    </div>
    @endsection