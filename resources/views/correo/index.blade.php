@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row justify-content-center">
    <div class="col-md-8 mb-4"><a class="btn btn-primary" href="{{url('correo/create')}}" >Crear  Correo</a>
        </br>
        </div>
         <div class="col-md-8">
     
    <h2 ><b>Lista Correo Creados:</b> </h2>

    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Empresa</th>
      <th scope="col">Fecha Creación</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @foreach ($correos as $correo)
      <th scope="row">{{ $correo->ca_nombre }}</th>
      <td>{{ $correo->ca_empresa }}</td>
      <td>{{ $correo->ca_fecha }}</td>
      <td><a class="btn btn-outline-info btn-sm" href="{{url('correo/'.$correo->id)}}" alt="ver correo"><i class="fa fa-eye" aria-hidden="true"></i></a>  <a class="btn btn-outline-success btn-sm" href="{{url('correo/'.$correo->id.'/edit')}}" alt="editar correo"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
    </tbody>
  
  </table>
{{ $correos->links() }}
        </div>
    </div>
</div>
@endsection
