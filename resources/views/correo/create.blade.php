@extends('layouts.app')

@section('content')
<div class="container">
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
  </div>
@endif
    <div class="row justify-content-center">
    <div class="col-md-8 mb-4">
        <a class="btn btn-primary " href="{{url('home')}}" >Volver a la lista</a>
      </div>
        <div class="col-md-8">
        <h2>Crear Correo</h2>
        </br>
        {!! Form::open(['route' => 'correo.store','method' => 'post']) !!}
        <div class="row">
        <div class="col-md-6 form-group">
        {!! Form::label('ca_fecha','Fecha:')!!}
        {!! Form::text('ca_fecha','',['id'=>'ca_fecha','class'=>'form-control'])!!}
        </div>
        <div class="col-md-6 form-group">
        {!! Form::label('ca_genero','Título:')!!}
        {!! Form::select('ca_genero',['Señor' => 'Señor', 'Señora' => 'Señora'] , null,['id'=>'ca_genero','class'=>'form-control'])!!}
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 form-group">
        {!! Form::label('ca_empresa','Nombre de Empresa:')!!}
        {!! Form::text('ca_empresa','',['id'=>'ca_empresa','class'=>'form-control'])!!}
        </div>
        <div class="col-md-6 form-group">
        {!! Form::label('ca_nombre','Nombre a quien va dirigido:')!!}
        {!! Form::text('ca_nombre','',['id'=>'ca_nombre','class'=>'form-control'])!!}
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 form-group">
        {!! Form::label('ca_cargo','Cargo:')!!}
        {!! Form::text('ca_cargo','',['id'=>'ca_cargo','class'=>'form-control'])!!}
        </div>
        <div class="col-md-12 form-group">
        {!! Form::label('ca_contenido','Contenido:')!!}
        {!!    Form::textarea('ca_contenido', 'Estimado Lic:

Mediante la presente, me dirijo a su persona para hacerle llegar mis más cordiales saludos y los mejores deseos de éxito en las delicadas funciones que tiene a bien desempeñar en esta prestigiosa empresa.

Radio Móvil "La Carroza", con amplia experiencia en el servicio de transporte de personas y mensajería, pone a su disposición toda su infraestructura y recurso humano a fin de coadyuvar satisfactoriamente al desarrollo de sus actividades. Somos una empresa dinámica y responsable, integrada por personal calificado para otorgar el servicio de calidad, eficiente y seguro que nuestros clientes merecen.', [
                    'class'      => 'form-control',
                    'rows'       => 7, 
                    'name'       => 'ca_contenido',
                    'id'         => 'ca_contenido',
                ])!!}
        </div>

        </div>
        
        {!! Form::submit('Crear',['class'=>'btn btn-primary'])!!}
        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
