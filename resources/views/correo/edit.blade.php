@extends('layouts.app')

@section('content')
<div class="container">
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
  </div>
@endif
    <div class="row justify-content-center">
    <div class="col-md-8 mb-4">
        <a class="btn btn-primary " href="{{url('home')}}" >Volver a la lista</a>
      </div>
        <div class="col-md-8">
        <h2>Crear Correo</h2>
        </br>
        {!! Form::open(['route' => 'correo.update','method' => 'post']) !!}
        <div class="row">
        {!! Form::hidden('correo_id',$correo->id)!!}
        <div class="col-md-6 form-group">
        {!! Form::label('ca_fecha','Fecha:')!!}
        {!! Form::text('ca_fecha',$correo->ca_fecha,['id'=>'ca_fecha','class'=>'form-control'])!!}
        </div>
        <div class="col-md-6 form-group">
        {!! Form::label('ca_genero','Título:')!!}
        {!! Form::select('ca_genero',['Señor' => 'Señor', 'Señora' => 'Señora'] , $correo->ca_genero,['id'=>'ca_genero','class'=>'form-control'])!!}
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 form-group">
        {!! Form::label('ca_empresa','Nombre de Empresa:')!!}
        {!! Form::text('ca_empresa',$correo->ca_empresa,['id'=>'ca_empresa','class'=>'form-control'])!!}
        </div>
        <div class="col-md-6 form-group">
        {!! Form::label('ca_nombre','Nombre a quien va dirigido:')!!}
        {!! Form::text('ca_nombre',$correo->ca_nombre,['id'=>'ca_nombre','class'=>'form-control'])!!}
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 form-group">
        {!! Form::label('ca_cargo','Cargo:')!!}
        {!! Form::text('ca_cargo',$correo->ca_cargo,['id'=>'ca_cargo','class'=>'form-control'])!!}
        </div>
        <div class="col-md-12 form-group">
        {!! Form::label('ca_contenido','Contenido:')!!}
        {!!    Form::textarea('ca_contenido', $correo->ca_contenido, [
                    'class'      => 'form-control',
                    'rows'       => 7, 
                    'name'       => 'ca_contenido',
                    'id'         => 'ca_contenido',
                ])!!}
        </div>

        </div>
        
        {!! Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

