@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 mb-4">
        <a class="btn btn-primary " href="{{url('home')}}" >Volver a la lista</a>
      </div>
        <div class="col-md-10">
            <h2><b>Detalle Correo</b></h2>
        <br/>
        <table style="width:600px;background-color:#ffffff;" id="signature_container"  cellspacing="0" cellpadding="0">
            <tbody>

              <tr >
                <td colspan="3" style="padding:30px; background-color:#dde7f0; text-align:center;" ><img style="text-align:center; " id="logo1" src="https://getserver.com.bo//recursos-la-carroza/carroza-logo-350.png" >
                </td>
              </tr>

              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px; padding-top:20px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">Santa Cruz, {{$correo->ca_fecha}}
                </td>
              </tr>

              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-top:20px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">{{$correo->ca_genero}}:
                </td>
              </tr>

              <tr> 
                <td colspan="3"  style="padding-left:10px;  font-family: 'Verdana'; font-size:14.4px; color:#001679; font-weight: 800;">{{$correo->ca_empresa}}
                </td>
              </tr>

              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">{{$correo->ca_nombre}}
                </td>
              </tr>

              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px; padding-bottom:20px;  font-family: 'Verdana'; font-size:14.4px; color:#001679; font-weight: 800; text-transform:uppercase">{{$correo->ca_cargo}}
                </td>
              </tr>

              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">Presente.-
                <br>
                <br>
                </td>
                </tr>
  
              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px;  font-family: 'Verdana'; font-size:14.4px; color:#001679; background:#001679;">&nbsp
                </td>
              </tr> 
               <tr> 
                <td style="padding-left:10px; padding-bottom:1px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">              <br>

<br>
{!! nl2br($correo->ca_contenido)!!}
                </td>
                
              </tr>  
        <tr>
        <td style="text-align:center;padding-bottom:20px;padding-top:20px;"> 
       	<table cellspacing="2" cellpadding="2" > 
       	<tr style="background-color:#cdcdcd; padding:30px; border: 1px solid #ffffff "> 
                <td  style="padding:20px;  font-family: 'Verdana'; font-size:14.4px; color:#001679; text-align:center;">
                <img  style="text-align:center;" src="https://getserver.com.bo//recursos-la-carroza/taxi.png" ></br>
                <b>Radio Móvil</b></br>
                Conozca nuestro sistema de crédito corporativo, para el transporte de su personal y clientes. Servicio 24/7.
                </td>

                <td  style=" text-align:center; padding:20px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">
                <img  style="text-align:center;" src="https://getserver.com.bo//recursos-la-carroza/moto.png" ></br>
                <b>Moto Express</b></br>
                Servicio de mensajería, envío de encomiendas y pago de servicios de la manera más rápida y segura.
                </td>
                <td  style="text-align:center; padding:20px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">
                <img  style="text-align:center;" src="https://getserver.com.bo//recursos-la-carroza/app.png" ></br>
                <b>APP</b></br>
                Nuestra aplicación móvil le permite a tus usuarios solicitar móvil o moto express de forma sencilla
                </td>
       	</tr> 

       	</table> 
   	</td>
        </tr>
              <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px; padding-top:20px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">Nuestra central se encuentra ubicada en la Av. Roca coronado 4to anillo calle Juan Bustamante 3070.
                <br>
              <br>
              Contamos con 120 unidades disponible para su servicio, con todas la medidas de bioseguridad, moto para mensajería, pago de servicio y compras. <br>
              <br>
Nuestro Servicio está disponible las 24 horas.    
 <br><br>Nuestros números telefónicos son: 3 518181 - 3 538181 - 691 11113 Whatsapp<br><br>

Las tarifas se encuentran entre las mejores del mercado en el rubro de transporte.
A la espera de formar parte de su selecto grupo de proveedores, quedo a su entera disposición ante cualquier consulta que requiera su empresa.
<br><br>
Sin otro particular me despido muy cordialmente.
<br>
<br>
<br>
Atentamente,</td>
</tr> 
<tr> 
<td colspan="3"  style="text-align:center;padding-left:10px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">
<br>
Rolando Edwin Endara Y.

</td>
 </tr> 
 <tr> 
<td colspan="3"  style="text-align:center;padding-left:10px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;font-weight: 800;">PROPIETARIO <br>

  <br></td>
 </tr> 
 
 <tr>
        <td style="text-align:center;padding-bottom:20px;padding-top:20px;"> 
       	<table cellspacing="0" cellpadding="0" > 
       	<tr style="text-align:center;"> 
            <td  style="witdh:70%; padding:20px; font-family: 'Verdana'; font-size:18px; color:#001679;font-weight: 800;">
               Para saber más acerca  de nuestros servicios        
            </td>
            <td  style="padding:5px; font-family: 'Verdana'; font-size:10px; font-weight: 800;color:#ffffff; background-color:#90C338;">
                <a style="border-radius:20px;text-align:center; font-family: 'Verdana'; color:#ffffff;" href="https://www.radiotaxilacarroza.com" target="_blank">VISITA NUESTRO<br> SITIO WEB</a>   </td>
       	</tr> 
       	</table> 
   	</td>
        </tr>
 <tr> 
                <td colspan="3"  style="padding-left:10px; padding-bottom:1px; font-family: 'Verdana'; font-size:14.4px; color:#001679; font-weight: 800;">CONTACTO</br></br></td>
              </tr>
              <tr> 
              <td colspan="3"  style="padding-left:10px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">
              <img  src="https://getserver.com.bo//recursos-la-carroza/mark.png" > Av. Roca coronado 4to anillo calle Juan Bustamante 3070.
              </td>
              </tr>
              <tr> 
              <td colspan="3"  style="padding-left:10px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">
              <img  src="https://getserver.com.bo//recursos-la-carroza/phone.png" > 3518181  -- 3538181
              </td>
              </tr> 
              <tr> 
              <td colspan="3"  style="padding-left:10px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">
              <img  src="https://getserver.com.bo//recursos-la-carroza/wsp.png" > 69111113
              </td>
              </tr> 
              <tr> 
              <td colspan="3"  style="padding-left:10px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">
              <img  src="https://getserver.com.bo//recursos-la-carroza/mail.png" > administracion@radiotaxilacarroza.com
              </td>
              </tr> 
              <tr> 
              <td colspan="3"  style="padding-left:10px; font-family: 'Verdana'; font-size:14.4px; color:#001679;">
              <img  src="https://getserver.com.bo//recursos-la-carroza/internet.png" > <a style="  font-family: 'Verdana'; font-size:14.4px; color:#001679;" href="https://www.radiotaxilacarroza.com" target="_blank">www.radiotaxilacarroza.com</a>
              </td>
              </tr> 
              <tr> 
              <td colspan="3"  style="padding-left:10px;  font-family: 'Verdana'; font-size:14.4px; color:#001679;">
              <img  src="https://getserver.com.bo//recursos-la-carroza/fb.png" > <a style=" font-family: 'Verdana'; font-size:14.4px; color:#001679;" href="https://fb.me/radiomovillacarroza" target="_blank">fb.me/radiomovillacarroza</a>
              </td>
              </tr>  
              </tbody>
              </table>
            <br><br>
            <input type="button" onclick="selectElementContents( document.getElementById('signature_container') );" value="Copiar Correo" class="btn btn-primary">
        </div>

    </div>
</div>

@endsection

@section('jsscript')
<script>
     function selectElementContents(el) {
    var body = document.body,
      range, sel;
    if (document.createRange && window.getSelection) {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      range.selectNodeContents(el);
      sel.addRange(range);
    }
    document.execCommand("Copy");
    alert('Copiado');
  }
</script>
@endsection