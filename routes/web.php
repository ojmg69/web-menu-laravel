<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/logout',   function () {
    //logout user
    Auth::logout();
    // redirect to homepage
    return redirect('/');
});

Auth::routes();
//Registro
//Route::get('/register', 'Auth\RegisterController@register')->name('register');
//Route::post('/register', 'Auth\RegisterController@create');
Auth::routes(["/register" => false]);

Route::get('/home', 'HomeController@panel')->name('home.panel');

//Correo
Route::get('/correo', 'CorreoController@index')->name('correo.index');
Route::get('/correo/create', 'CorreoController@create')->name('correo.create');
Route::post('/correo/store', 'CorreoController@store')->name('correo.store');
Route::get('/correo/{id}', 'CorreoController@show')->name('correo.show');
Route::get('/correo/{id}/edit', 'CorreoController@edit')->name('correo.edit');
Route::post('/correo/update', 'CorreoController@update')->name('correo.update');

//Categoria
Route::get('/categoria', 'CategoriaController@index')->name('categoria.index');
Route::get('/categoria/create', 'CategoriaController@create')->name('categoria.create');
Route::post('/categoria/store', 'CategoriaController@store')->name('categoria.store');
Route::get('/categoria/{id}/edit', 'CategoriaController@edit')->name('categoria.edit');
Route::post('/categoria/update', 'CategoriaController@update')->name('categoria.update');
Route::delete('categoria/{id}', 'CategoriaController@destroy')->name('categoria.destroy');

//Cambio de contraseña
Route::get('/contrasena', 'ChangePasswordController@index')->name('contrasena.index');
Route::post('/contrasena/update', 'ChangePasswordController@update')->name('contrasena.update');

//Perfil Empresa
Route::get('/perfil', 'CompanyController@index')->name('perfil.index');
Route::get('/perfil/{id}/edit', 'CompanyController@edit')->name('perfil.edit');
Route::post('/perfil/update', 'CompanyController@update')->name('perfil.update');

//Producto
Route::get('/producto', 'ProductController@index')->name('producto.index');
Route::get('/producto/{id}/show', 'ProductController@show')->name('producto.show');
Route::get('/producto/create', 'ProductController@create')->name('producto.create');
Route::post('/producto/store', 'ProductController@store')->name('producto.store');
Route::get('/producto/{id}/edit', 'ProductController@edit')->name('producto.edit');
Route::post('/producto/update', 'ProductController@update')->name('producto.update');
Route::delete('/producto/{id}', 'ProductController@destroy')->name('producto.destroy');