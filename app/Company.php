<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name', 'address', 'about', 'provincia', 'canton', 'teluno', 'teldos', 'image'
        ];
}
