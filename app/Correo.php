<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'ca_fecha', 'ca_genero', 'ca_empresa','ca_nombre','ca_cargo','ca_contenido'
    ];
}
