<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Company;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $company=Company::first();
        $categories = Category::all()->sortBy("id");
        $products = Product::all()->where('active',1)->sortBy("category_id");
        $company_name=$company->name;
        //dd($categories);
        return view('home',['company'=>$company_name,'categories'=>$categories,'products'=>$products]);
    }
    public function panel()
    {
        $total_productos=Product::count();
        $total_categoria=Category::count();
        return view('panel',['total_productos'=>$total_productos,'total_categoria'=>$total_categoria]);
    }
}
