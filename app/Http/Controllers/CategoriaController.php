<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
class CategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    //$categorias=Category::paginate(20);
    $as_categorias=DB::select(DB::raw("SELECT * FROM categories WHERE categories.id NOT IN (SELECT DISTINCT products.category_id FROM products WHERE categories.id = products.category_id)"));
    //$con_categorias=DB::select(DB::raw("SELECT *FROM products LEFT JOIN categories ON products.category_id = categories.id ORDER BY products.id DESC"));
    $con_categorias=DB::select(DB::raw("SELECT DISTINCT categories.id, categories.name FROM categories INNER JOIN products ON categories.id=products.category_id ORDER by categories.id ASC"));
    
    if($as_categorias==null  ||$con_categorias==null ){
        return view('categoria.create');
    }else{
        $todos = new Paginator($as_categorias, 20);
        $c_categoria = new Paginator($con_categorias, 20);
    
        return view('categoria.index',['categorias'=>$todos,'con_categorias'=>$c_categoria]);
    }
}
public function create(){
        return view('categoria.create');
    }

    public function store(Request $request){
        $categoria = new Category;
        $v = Validator::make($request->all(), [
            
            'name' => 'required|unique:categories'
        ],
        ['name.required' => 'El campo Nombre es obligatorio',
        'name.unique' => 'El campo Nombre ya existe']

    );
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $categoria->name = $request->name;
        $categoria->save();
        return redirect()->route('categoria.index');
    }


    public function update(Request $request){
        $categoria = Category::find($request->categoria_id);
        $v = Validator::make($request->all(), [
            
            'name' => 'required|unique:categories'
        ],
        ['name.required' => 'El campo Nombre es obligatorio',
        'name.unique' => 'El campo Nombre ya existe']

    );
    if ($v->fails())
    {
        return redirect()->back()->withInput()->withErrors($v->errors());
    }
    $categoria->name = $request->name;
    $categoria->save();
    return redirect()->route('categoria.index');   
    }

    public function edit($id){
        $categoria = Category::findOrFail($id);
        return view('categoria.edit',['categoria'=>$categoria]);
    }
    public function destroy($id){
        $categoria = Category::find($id);
        $categoria->delete();
//        return redirect('/');
        return redirect()->route('categoria.index');
    }

}
