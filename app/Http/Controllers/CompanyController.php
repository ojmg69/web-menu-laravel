<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $company=Company::first();
        $user_id=Auth::id();
        $user=User::findOrFail($user_id);
        $user_email=$user->email;
      return view('perfil.index',['company'=>$company,'user_email'=>$user_email]);
    }

    protected function update(Request $request)
    {
        $imageName =  'logo.png';
        $perfil = Company::find($request->perfil_id);
        if($request->Imagecroped!==null){
            $image = $request->Imagecroped;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            
            Storage::disk('public')->put('img/profile/'.$imageName, base64_decode($image));

    
        }
        $v = Validator::make($request->all(), [
            'imagen' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ],
        ['imagen.image' => 'El formato de imagen no es permitido',
        'imagen.mimes' => 'El formato de imagen no es permitido',
        'imagen.max' => 'Solo es permitido imagen maximo de 2mb']

    );
        
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{
            //$img= $request->file('imagen');
            //$nombre= time().'.'.$img->getClientOriginalExtension();
            //$nombre= 'logo.'.$img->getClientOriginalExtension();
            //$destino= public_path('img/profile');
            //$request->imagen->move($destino,$nombre);
            $perfil->name = $request->name;
            $perfil->address = $request->address;
            $perfil->about = $request->about;
            $perfil->provincia = $request->provincia;
            $perfil->canton = $request->canton;
            $perfil->teluno = $request->teluno;
            $perfil->teldos = $request->teldos;
            $perfil->image = $imageName;
            $perfil->save();
        return redirect('perfil');
      }
        
    }

    public function edit($id){
        $company = Company::findOrFail($id);
        return view('perfil.edit',['company'=>$company]);
    }
}
