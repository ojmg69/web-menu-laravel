<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        
        $sql_products=DB::select(DB::raw("SELECT products.id, products.name, products.active, products.description, products.price, categories.name AS category FROM products LEFT JOIN categories ON products.category_id = categories.id ORDER BY products.id DESC"));
        //dd($sql_products);
        if($sql_products==null){
            $categorias=Category::pluck('name','id');
            return view('producto.create',['categorias'=>$categorias]);
        }else{
            $products = new Paginator($sql_products, 20);
            return view('producto.index',['products'=>$products]);
        }
        
        
    }

    public function show($id){
        $product=DB::select(DB::raw("SELECT products.id, products.name, products.image, products.active, products.description, products.price, categories.name AS category FROM products LEFT JOIN categories ON products.category_id = categories.id WHERE products.id=$id"));
        return view('producto.show',['product'=>$product[0]]);

    }

    public function create(){
        $categorias=Category::pluck('name','id');
        return view('producto.create',['categorias'=>$categorias]);

    }

    public function store(Request $request){
    
        $product = new Product;


        $v = Validator::make($request->all(), [
            
            'name' => 'required|unique:categories',
            'price' => 'required',
            'category_id' => 'required'
        ],
        ['name.required' => 'El campo Nombre es obligatorio',
        'price.required' => 'El campo Precio es obligatorio',
        'category_id.required' => 'El campo categoria es obligatorio',
        'name.unique' => 'El campo Nombre ya existe']

    );
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        
        $product->name = $request->name;
        $product->description = $request->description;
        $product->options = $request->options;
        $product->extras = $request->extras;
        $product->price = $request->price;
        $product->active = $request->active ? '1' : '0';
        $product->category_id = $request->category_id;
        $product->save();
        $product = Product::findOrFail($product->id);

        $imageName =  'image'.$product->id.'.png';
        if($request->Imagecroped!==null){
            $image = $request->Imagecroped;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            
            Storage::disk('public')->put('img/products/'.$imageName, base64_decode($image));
        }
        $product->image = ($request->Imagecroped!==null)?$imageName: 'image1.png';
        $product->save();
        return redirect()->route('producto.index');
    }


    public function update(Request $request){
        $product = Product::find($request->product_id);
       
        $v = Validator::make($request->all(), [
            
            'name' => 'required|unique:categories',
            'price' => 'required',
            'category_id' => 'required'
        ],
        ['name.required' => 'El campo Nombre es obligatorio',
        'price.required' => 'El campo Precio es obligatorio',
        'category_id.required' => 'El campo categoria es obligatorio',
        'name.unique' => 'El campo Nombre ya existe']

    );
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        
        $product->name = $request->name;
        $product->description = $request->description;
        $product->options = $request->options;
        $product->extras = $request->extras;
        $product->price = $request->price;
        $product->active = $request->active ? '1' : '0';
        $product->category_id = $request->category_id;
        $product->save();
        $product = Product::findOrFail($product->id);

        $imageName =  'image'.$product->id.'.png';
        if($request->Imagecroped!==null){
            $image = $request->Imagecroped;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            
            Storage::disk('public')->put('img/products/'.$imageName, base64_decode($image));
        }
        //$product->image = ($request->Imagecroped!==null)? $imageName: 'image1.png';
        $product->image = $imageName;
        
        $product->save();
        return redirect()->route('producto.index');
    
    }


    public function edit($id){
        $product = Product::findOrFail($id);
        $categorias=Category::pluck('name','id');
        return view('producto.edit',['product'=>$product,'categorias'=>$categorias]);
    }

    public function destroy($id){
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('producto.index');
    }
}
