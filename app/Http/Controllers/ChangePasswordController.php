<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Providers\RouteServiceProvider;
class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        //$categoria = Category::findOrFail($id);

        $user_id=Auth::id();
        $user=User::findOrFail($user_id);
        $user_email=$user->email;
        
       return view('contrasena.edit',['user_id'=>$user_id,'user_email'=>$user_email]);
    }

    protected function update(Request $request)
    {
        $user = User::find($request->user_id);
        $user->email = $request->email;
        $user->password = Hash::make($request['password']);
        $user->save();
        return redirect('home');
    }
}
