<?php

namespace App\Http\Controllers;
use App\Correo;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class CorreoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    $correos=Correo::paginate(20);
    return view('correo.index',['correos'=>$correos]);
}
    public function create(){
        return view('correo.create');
    }
    public function store(Request $request){
        $correo = new Correo;
        $v = Validator::make($request->all(), [
            
            'ca_fecha' => 'required',
            'ca_empresa' => 'required',
            'ca_nombre' => 'required',
            'ca_cargo' => 'required',
        ],
        ['ca_fecha.required' => 'El campo Fecha es obligatorio',
        'ca_empresa.required' => 'El campo Nombre empresa es obligatorio',
        'ca_nombre.required' => 'El campo Nombre a quien va dirigido es obligatorio',
        'ca_cargo.required' => 'El campo Cargo es obligatorio',
        ]

    );
 
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $correo->ca_fecha = $request->ca_fecha;
        $correo->ca_genero = $request->ca_genero;
        $correo->ca_empresa = $request->ca_empresa;
        $correo->ca_nombre = $request->ca_nombre;
        $correo->ca_cargo = $request->ca_cargo;
        $correo->ca_contenido = $request->ca_contenido;
        $correo->save();
        return redirect()->route('correo.show', ['id' => $correo->id]);
    }

    public function update(Request $request){
        $correo = Correo::find($request->correo_id);
        $v = Validator::make($request->all(), [
            
            'ca_fecha' => 'required',
            'ca_empresa' => 'required',
            'ca_nombre' => 'required',
            'ca_cargo' => 'required',
        ],
        ['ca_fecha.required' => 'El campo Fecha es obligatorio',
        'ca_empresa.required' => 'El campo Nombre empresa es obligatorio',
        'ca_nombre.required' => 'El campo Nombre a quien va dirigido es obligatorio',
        'ca_cargo.required' => 'El campo Cargo es obligatorio',
        ]

    );
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $correo->ca_fecha = $request->ca_fecha;
        $correo->ca_genero = $request->ca_genero;
        $correo->ca_empresa = $request->ca_empresa;
        $correo->ca_nombre = $request->ca_nombre;
        $correo->ca_cargo = $request->ca_cargo;
        $correo->ca_contenido = $request->ca_contenido;
        $correo->save();
        return redirect()->route('correo.index');        
    }

    public function show($id){
        $correo = Correo::findOrFail($id);

        return view('correo.show',['correo'=>$correo]);
    }

    public function edit($id){
        $correo = Correo::findOrFail($id);
        return view('correo.edit',['correo'=>$correo]);
    }

}
